package examples.jdbc.integration;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.sql.SQLException;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import examples.jdbc.operations.primitiveOperations.TransactionManager;

public class TestDeptDAO {

	private DeptDAO target;
	private TransactionManager mana;
	
	@BeforeEach
	void setUp() throws SQLException{
		target = new DeptDAO();
		try {
			mana = new JDBCTransactionFactry().create();
		}catch(Exception e) {
			mana.close();
			throw e;
		}
	}
	
	@AfterEach
	void tearDown() throws SQLException{
		try {
			mana.close();
		}finally {
			mana.close();
		}
	}
	
	
	@Test void read() {
		    var d = target.read(mana, 9999);
		    assertTrue(d.isEmpty());	 
	}
}
