package examples.jdbc.operations.primitiveOperations;

import java.sql.SQLException;
import java.util.Optional;

public interface CRUD<T> {
	int create(TransactionManager man, T t) throws SQLException;
	Optional<T> read(TransactionManager man, int id) throws SQLException;
	int update(TransactionManager man, T t) throws SQLException;
	int delete(TransactionManager man, int id) throws SQLException;
}
