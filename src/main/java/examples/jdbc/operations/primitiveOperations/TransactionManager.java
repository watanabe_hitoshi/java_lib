package examples.jdbc.operations.primitiveOperations;

public interface TransactionManager {
	public static final boolean SUCCESS=true;
	public static final boolean FAIL=false;
	
	boolean setTransaction();
	boolean rollback();
	boolean commit();
	void close();
	
}
