package examples.jdbc.integration;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Optional;

import examples.jdbc.entity.Dept;
import examples.jdbc.exceptions.CannotCreateException;
import examples.jdbc.exceptions.CannotDeleteException;
import examples.jdbc.exceptions.CannotUpdateException;
import examples.jdbc.exceptions.DataAccessException;
import examples.jdbc.integration.JDBCTransactionFactry.JDBCTransactionManager;
import examples.jdbc.operations.primitiveOperations.CRUD;
import examples.jdbc.operations.primitiveOperations.TransactionManager;

public class DeptDAO implements SqlUtil, CRUD<Dept>{

	private enum Attrs{
		ID("deptno"), NAME("dname"), LOCATION("loc");
		
		private String col;
		private Attrs(String col) { this.col=col; }
		public String toString() { return col; }
	}
	
	@Override public int create(TransactionManager man, Dept d) {
		String sql = "INSERT INTO dept("+Attrs.ID+", "+Attrs.NAME+", "+Attrs.LOCATION+") VALUES(?,?,?)";
		String test = "SELECT ? FROM dept WHERE "+Attrs.ID+" = ?";
		var con = ((JDBCTransactionManager)man).connect();
		try(var pre = con.prepareStatement(sql);
				var pre2 = con.prepareStatement(test)){
			pre.setInt(1, d.deptno());
			pre.setString(2, d.dname());
			pre.setString(3, d.loc());
			pre.executeUpdate();
	
			var fetch = pre2.executeQuery();
			if(fetch.next())
				return fetch.getInt(Attrs.ID.toString());
			else
				throw new CannotCreateException(d.toString());
		}catch(SQLException ex) {
			throw new DataAccessException(ex);
		}
	}
	
	
	@Override public Optional<Dept> read(TransactionManager man, int id) {
		String sql = "SELECT * FROM dept WHERE "+Attrs.ID+" = ?";
		var con = ((JDBCTransactionManager)man).connect();
		try(var pre = con.prepareStatement(sql)){
			pre.setInt(1, id);
			var fetch = pre.executeQuery();
				return fetch.next()? makeResult(fetch): Optional.empty();
		}catch(SQLException ex) {
			throw new DataAccessException(ex);
		}
	}
	
	@Override public int update(TransactionManager man, Dept d) {
		String sql=String.format("UPDATE dept SET %s = ?, %s = ? WHERE %s = ?", Attrs.NAME, Attrs.LOCATION, Attrs.ID);
		var con = ((JDBCTransactionManager)man).connect();
		try(var pre = con.prepareStatement(sql)){
			pre.setString(1, d.dname());
			pre.setString(2, d.loc());
			pre.setInt(3, d.deptno());

			int res = pre.executeUpdate();
			if(res == 1) return res;
			else throw new CannotUpdateException(d.toString());
		}catch(SQLException ex) {
			throw new DataAccessException(ex);
		}
	}
	
	@Override public int delete(TransactionManager man, int id) {
		String sql=String.format("DELET FORM dept WHERE %s = ?", Attrs.ID);
		var con = ((JDBCTransactionManager)man).connect();
		try(var pre = con.prepareStatement(sql)){
			pre.setInt(1, id);
			
			int res = pre.executeUpdate();
			if(res == 1) return res;
			else throw new CannotDeleteException(Integer.toString(id));
		}catch(SQLException ex) {
			throw new DataAccessException(ex);
		}
	}
	
	private Optional<Dept> makeResult(ResultSet fetch)throws SQLException{
		 return Optional.of(Dept.of(
					fetch.getInt(Attrs.ID.toString()),
					fetch.getString(Attrs.NAME.toString()),
					fetch.getString(Attrs.LOCATION.toString())));
	}
	
}
