package examples.jdbc.integration;

public class Constants {
	public static final String URL;
	public static final String USER;
	public static final String PASS;
	
	
	static {
		URL="jdbc:postgresql:postgres";
		USER = "postgres";
		PASS = "postgres";
	}
	private Constants() {}
}
