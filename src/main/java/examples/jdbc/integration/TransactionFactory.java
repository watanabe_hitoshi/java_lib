package examples.jdbc.integration;

import examples.jdbc.operations.primitiveOperations.TransactionManager;

public interface TransactionFactory {
	TransactionManager create();
}
