package examples.jdbc.integration;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Optional;

import examples.jdbc.entity.Employee;
import examples.jdbc.exceptions.CannotCreateException;
import examples.jdbc.exceptions.CannotDeleteException;
import examples.jdbc.exceptions.CannotUpdateException;
import examples.jdbc.exceptions.DataAccessException;
import examples.jdbc.integration.JDBCTransactionFactry.JDBCTransactionManager;
import examples.jdbc.operations.primitiveOperations.CRUD;
import examples.jdbc.operations.primitiveOperations.TransactionManager;

public class EmpDAO implements SqlUtil, CRUD<Employee>{
	
	private enum Attrs{
		ID("empno"), NAME("ename"), JOB("job");
		
		private final String col;
		private Attrs(String col) { this.col=col; }
		public String toString() { return col; }
	}
	
	@Override public int create(TransactionManager man, Employee e) {
		String sql = "INSERT INTO emp("+Attrs.ID+", "+Attrs.NAME+", "+Attrs.JOB+") VALUES(?,?,?)";
		String test = "SELECT ? FROM emp WHERE "+Attrs.ID+" = ?";
		var con = ((JDBCTransactionManager)man).connect();
		try(var pre = con.prepareStatement(sql);
				var pre2 = con.prepareStatement(test)){
			pre.setInt(1, e.id());
			pre.setString(2, e.name());
			pre.setString(3, e.job());
			pre.executeUpdate();
	
			var fetch = pre2.executeQuery();
			if(fetch.next())
				return fetch.getInt(Attrs.ID.toString());
			else
				throw new CannotCreateException(e.toString());
		}catch(SQLException ex) {
			throw new DataAccessException(ex);
		}
	}
	
	@Override public Optional<Employee> read(TransactionManager man, int id) {
		String sql = "SELECT * FROM emp WHERE "+Attrs.ID+" = ?";
		var con = ((JDBCTransactionManager)man).connect();
		try(var pre = con.prepareStatement(sql)){
			pre.setInt(1, id);
			var fetch = pre.executeQuery();
				return fetch.next()? makeResult(fetch): Optional.empty();
		}catch(SQLException ex) {
			throw new DataAccessException(ex);
		}
	}
	
	@Override public int update(TransactionManager man, Employee e) {
		String sql=String.format("UPDATE emp SET %s = ?, %s = ? WHERE %s = ?", Attrs.NAME, Attrs.JOB, Attrs.ID);
		var con = ((JDBCTransactionManager)man).connect();
		try(var pre = con.prepareStatement(sql)){
			pre.setString(1, e.name());
			pre.setString(2, e.job());
			pre.setInt(3, e.id());

			int res = pre.executeUpdate();
			if(res == 1) return res;
			else throw new CannotUpdateException(e.toString());
		}catch(SQLException ex) {
			throw new DataAccessException(ex);
		}
	}
	
	@Override public int delete(TransactionManager man, int id) {
		String sql=String.format("DELET FORM emp WHERE %s = ?", Attrs.ID);
		var con = ((JDBCTransactionManager)man).connect();
		try(var pre = con.prepareStatement(sql)){
			pre.setInt(1, id);
			
			int res = pre.executeUpdate();
			if(res == 1) return res;
			else throw new CannotDeleteException(Integer.toString(id));
		}catch(SQLException ex) {
			throw new DataAccessException(ex);
		}
	}
	
	private Optional<Employee> makeResult(ResultSet fetch)throws SQLException{
		 return Optional.of(Employee.of(
					fetch.getInt(Attrs.ID.toString()),
					fetch.getString(Attrs.NAME.toString()),
					fetch.getString(Attrs.JOB.toString())));
	}
}

