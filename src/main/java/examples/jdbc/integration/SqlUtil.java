package examples.jdbc.integration;

public interface SqlUtil {
	public static final char ESC = '\\';
	public static final char WILDANY = '%';
	public static final char WILDONE = '_';
	
	default public String escape(String s) {
		StringBuilder res = new StringBuilder();
		for(int i=0; i<s.length(); i++) {
			res.append(switch(s.charAt(i)) {
				case ESC, WILDANY, WILDONE -> ESC + s.charAt(i);
				default -> s.charAt(i);
			});
		}
		return res.toString();
	}
}
