package examples.jdbc.integration;

import static examples.jdbc.integration.Constants.PASS;
import static examples.jdbc.integration.Constants.URL;
import static examples.jdbc.integration.Constants.USER;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import examples.jdbc.exceptions.DBConnectException;
import examples.jdbc.exceptions.DataAccessException;
import examples.jdbc.exceptions.TransactionExistException;
import examples.jdbc.operations.primitiveOperations.TransactionManager;

public class JDBCTransactionFactry implements TransactionFactory{
	public TransactionManager create() {
		return new JDBCTransactionManager();
	}
	
	static final class JDBCTransactionManager implements TransactionManager{
		private boolean existTransaction = false;
		private Connection con;
		
		{
			try(Connection con = DriverManager.getConnection(URL, USER, PASS)){
				this.con=con;
			}catch(SQLException ex) {
				throw new DBConnectException(ex);
			}
		}
			
		@Override public boolean setTransaction() {
			if(existTransaction) return FAIL;
			try{
				con.setAutoCommit(false);
				return SUCCESS;						
			}catch(SQLException ex) {
				throw new DataAccessException(ex);
			}
		}
		
		@Override public boolean rollback() {
			if(!existTransaction) return FAIL;
			try{
				con.rollback();
				return SUCCESS;						
			}catch(SQLException ex) {
				throw new DataAccessException(ex);
			}
		}
		
		@Override public boolean commit() {
			if(!existTransaction) return FAIL;
			try{
				con.commit();
				return SUCCESS;						
			}catch(SQLException ex) {
				throw new DataAccessException(ex);
			}
		}
		
		@Override public void close() {
			if(existTransaction) throw new TransactionExistException("commit or rollback");
			try {
				con.close();				
			}catch (SQLException ex){
				throw new DataAccessException(ex);
			}
		}
		
		Connection connect() {
			return con;
		}
	}
}
