package examples.jdbc.exceptions;

public class DBConnectException extends RuntimeException{
	  public DBConnectException(String message) {
		    super(message);
		  }

		  public DBConnectException(Throwable cause) {
		    super(cause);
		  }

		  public DBConnectException(String message, Throwable cause) {
		    super(message, cause);
		  }

}