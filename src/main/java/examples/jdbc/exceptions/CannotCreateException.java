package examples.jdbc.exceptions;

public class CannotCreateException extends RuntimeException{
	  public CannotCreateException(String message) {
		    super(message);
		  }

		  public CannotCreateException(Throwable cause) {
		    super(cause);
		  }

		  public CannotCreateException(String message, Throwable cause) {
		    super(message, cause);
		  }

}
