package examples.jdbc.exceptions;

public class CannotDeleteException extends RuntimeException{
	  public CannotDeleteException(String message) {
		    super(message);
		  }

		  public CannotDeleteException(Throwable cause) {
		    super(cause);
		  }

		  public CannotDeleteException(String message, Throwable cause) {
		    super(message, cause);
		  }

}
