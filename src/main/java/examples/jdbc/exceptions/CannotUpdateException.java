package examples.jdbc.exceptions;

public class CannotUpdateException extends RuntimeException{
	  public CannotUpdateException(String message) {
		    super(message);
		  }

		  public CannotUpdateException(Throwable cause) {
		    super(cause);
		  }

		  public CannotUpdateException(String message, Throwable cause) {
		    super(message, cause);
		  }

}
