package examples.jdbc.exceptions;

public class TransactionExistException extends RuntimeException{
	  public TransactionExistException(String message) {
		    super(message);
		  }

		  public TransactionExistException(Throwable cause) {
		    super(cause);
		  }

		  public TransactionExistException(String message, Throwable cause) {
		    super(message, cause);
		  }

}
