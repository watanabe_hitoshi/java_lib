package examples.jdbc.entity;

public class Employee {
	private final int id;
	private String name;
	private String job;
	
	private Employee(int id, String name, String job) {
		if(id<0) throw new  IllegalArgumentException();
		this.id = id;
		this.name = name;
		this.job = job;
	}
	
	public static Employee of(int id, String name, String pos) {
		return new Employee(id, name, pos);
	}

	public int id() {
		return id;
	}
	
	public String name() {
		return name;
	}
	
	public String job() {
		return job;
	}
	
	@Override public String toString() {
		return "id:"+id()+", name:"+name()+", job:"+job();
	}
	
	@Override public boolean equals(Object that) {
		if(that == this) return true;
		else if(that instanceof Employee && ((Employee)that).id()==id() ) return true;
		else return false;
	}
	
	@Override public int hashCode() {
		return Integer.hashCode(id());
	}
}
