package examples.jdbc.entity;

public class Dept {
	private final int deptno;
	private String dname;
	private String loc;
	
	private Dept(int deptno, String dname, String loc) {
		if (deptno < 0) throw new IllegalArgumentException();
		this.deptno=deptno;
		this.dname=dname;
		this.loc=loc;
	}
	
	public static final Dept of(int deptno, String dname, String loc) {
		return new Dept(deptno, dname, loc);
	}
	
	
	public int deptno() {
		return deptno;
	}
	
	public String dname() {
		return dname;
	}
	
	public String loc() {
		return loc;
	}


	@Override public String toString() {
		return "No:"+ deptno+", name:"+dname+", location:"+loc+";";
	}
}


