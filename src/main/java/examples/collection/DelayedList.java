package examples.collection;

import static examples.collection.List.nil;
import java.util.function.Supplier;
import java.util.function.UnaryOperator;


/**
 * 遅延評価{@link List リスト}.
 *
 * <p>
 * 遅延評価リストの中間操作 (<i>intermediate operation</i> - リストを返す操作)
 * は{@link #head 先頭要素}を特定するために必要な計算のみを実行し,
 * 後続要素の特定を{@link #tail 取得時}まで延期する
 * (先頭要素を特定するために全要素を操作する必要がある中間操作は即時評価される).
 * 遅延評価は無限列の表現を可能にする.
 * </p>
 *
 * <p>
 * このインターフェイスの static ファクトリメソッドはデフォルト実装のインスタンスを構築する.
 * </p>
 *
 * @param <E> 要素の型
 * @see <a href="https://mitpress.mit.edu/sites/default/files/sicp/full-text/book/book-Z-H-24.html">
 *        <i>Structure and Interpretation of Computer Programs - 3.5 Streams</i>
 *      </a>
 */
public interface DelayedList<E> extends List<E> {

  //// static ファクトリメソッド ////

  /**
   * 与えられた先頭要素と後続リストを与える約束からリストを構成する.
   *
   * @param  <T>     要素の型
   * @param  head    先頭要素
   * @param  promise 後続リストを与える約束
   * @return 先頭要素と未解決な後続リストから成るリスト
   * @throws NullPointerException {@code promise} が {@code null} の場合
   */
  static <T> List<T> cons(T head, Supplier<? extends List<? extends T>> promise) {
    return new DelayedListImpl<>(head, promise);
  }

  /**
   * 生成器によって無限リストを構成する.
   *
   * @param  <T>       要素の型
   * @param  generator 生成器
   * @return 生成器が与える要素の列
   */
  static <T> List<T> generate(Supplier<? extends T> generator) {
    return cons(generator.get(), () -> generate(generator));
  }

  /**
   * 前の要素から次の要素を計算する生成器によって無限リストを構成する.
   * 要素の並びは次のようである:
   * {@code initial, incr(initial), incr(incr(initial)), ...}
   *
   * @param  <T>       要素の型
   * @param  initial   初期値
   * @param  generator 生成器
   * @return 生成器が与える要素の列
   */
  static <T> List<T> iterate(T initial, UnaryOperator<T> generator) {
    return cons(initial, () -> iterate(generator.apply(initial), generator));
  }

  /**
   * 与えられた要素を含むリストを構成する.
   *
   * @param  <T> 要素の型
   * @param  xs  リストに含むべき要素の列
   * @return {@code xs} のリスト表現 ({@code xs} が空であれば {@code nil})
   */
  @SafeVarargs static <T> List<T> of(T... xs) {
    return of(xs, 0);
  }

  private static <T> List<T> of(T[] xs, int index) {
    return index < xs.length ? cons(xs[index], () -> of(xs, index + 1)) : nil();
  }

}
