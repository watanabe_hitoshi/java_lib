package examples.collection;

import static examples.collection.List.nil;
import static java.util.Objects.requireNonNull;
import java.util.Objects;
import java.util.Optional;
import java.util.StringJoiner;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;


class DelayedListImpl<E> extends AbstractList<E> {

  private Supplier<List<E>> promise;
  private Optional<List<E>> tail;

  @SuppressWarnings("unchecked") // List は不変であるためヒープ汚染は起こり得ない
  DelayedListImpl(E head, List<? extends E> tail) {
    super(head);
    this.tail = Optional.of((List<E>)tail);
  }

  @SuppressWarnings("unchecked") // List は不変であるためヒープ汚染は起こり得ない
  DelayedListImpl(E head, Supplier<? extends List<? extends E>> promise) {
    super(head);
    this.promise = (Supplier<List<E>>)requireNonNull(promise, "`promise' must not be null.");
    this.tail = Optional.empty();
  }

  @Override <T> List<T> cons(T head, List<? extends T> tail) {
    return new DelayedListImpl<>(head, tail);
  }

  <T> List<T> cons(T head, Supplier<? extends List<? extends T>> promise) {
    return new DelayedListImpl<>(head, promise);
  }

  @Override public boolean equals(Object that) {
    if (that == this)
      return true;
    else if (that != null && that.getClass() == getClass()) {
      var x = (DelayedListImpl<?>)that;
      return Objects.equals(head(), x.head()) && tailEquals(x);
    }
    else
      return false;
  }

  private boolean tailEquals(DelayedListImpl<?> that) {
    return tail.isEmpty() ?
             that.tail.isEmpty() ? promise.equals(that.promise) : false :
             that.tail.isEmpty() ? false : tail().equals(that.tail());
  }

  @Override public int hashCode() {
    return Objects.hash(head(), tail.isEmpty() ? promise : tail());
  }

  @Override public String toString() {
    var joiner = new StringJoiner(", ", "(", ")");
    joiner.add(Objects.toString(head()));
    joiner.add(tail.map(Object::toString).orElse("..."));
    return joiner.toString();
  }

  //// プリミティブ ////

  @Override public List<E> tail() {
    return tail.orElseGet(() -> (tail = Optional.of(promise.get())).get());
  }

  //// 中間操作 ////

  @Override public List<E> filter(Predicate<? super E> predicate) {
    return isNil() ?
             this :
             predicate.test(head()) ?
               cons(head(), () -> tail().filter(predicate)) :
               tail().filter(predicate);
  }

  @Override public <R> List<R> map(Function<? super E, ? extends R> transducer) {
    return isNil() ? nil() : cons(transducer.apply(head()), () -> tail().map(transducer));
  }

}
