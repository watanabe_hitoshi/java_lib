package examples.collection;

import static examples.collection.List.nil;
import java.util.Objects;
import java.util.StringJoiner;
import java.util.function.Function;
import java.util.function.Predicate;


abstract class AbstractList<E> implements List<E> {

  private final E head;

  AbstractList(E head) {
    this.head = head;
  }

  abstract <T> List<T> cons(T head, List<? extends T> tail);

  @Override public boolean equals(Object that) {
    if (that == this)
      return true;
    else if (that != null && that.getClass() == getClass()) {
      var x = (List<?>)that;
      return Objects.equals(head(), x.head()) && tail().equals(x.tail());
    }
    else
      return false;
  }

  @Override public int hashCode() {
    return Objects.hash(head(), tail());
  }

  @Override public String toString() {
    var joiner = new StringJoiner(", ", "(", ")");
    joiner.add(Objects.toString(head()));
    joiner.add(tail().toString());
    return joiner.toString();
  }

  @Override public E       head()  { return head; }
  @Override public boolean isNil() { return false; }

  @Override public List<E> filter(Predicate<? super E> predicate) {
    return isNil() ?
             this :
             predicate.test(head()) ?
               cons(head(), tail().filter(predicate)) :
               tail().filter(predicate);
  }

  @Override public <R> List<R> map(Function<? super E, ? extends R> transducer) {
    return isNil() ? nil() : cons(transducer.apply(head()), tail().map(transducer));
  }

}
