package examples.collection;

import static java.util.Objects.requireNonNull;
import java.util.NoSuchElementException;


class ListImpl<E> extends AbstractList<E> {

  // end-of-list marker
  static final List<?> NIL = new ListImpl<>() {
    @Override public Object head()               { throw new NoSuchElementException(); }
    @Override public List<Object> tail()         { throw new UnsupportedOperationException(); }
    @Override public boolean isNil()             { return true; }
    @Override public boolean equals(Object that) { return that == this; }
    @Override public int hashCode()              { return System.identityHashCode(this); }
    @Override public String toString()           { return "()"; }
  };

  private final List<E> tail;

  private ListImpl() { // NIL のためのコンストラクタ
    super(null);
    tail = null;
  }

  @SuppressWarnings("unchecked") // List は不変であるためヒープ汚染は起こり得ない
  ListImpl(E head, List<? extends E> tail) {
    super(head);
    this.tail = (List<E>)requireNonNull(tail, "`tail' must not be null.");
  }

  @Override <T> List<T> cons(T head, List<? extends T> tail) {
    return new ListImpl<>(head, tail);
  }

  @Override public List<E> tail() { return tail; }

}
