package examples.collection;

import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;


/**
 * 等質 (<i>homogeneous</i>) な LISP 風リスト.
 *
 * <p>
 * <em>リストのインスタンスは不変でなければならない.</em>
 * ここでは不変とは構築時にリストが保持する要素への参照が論理的に決まり,
 * それらの参照が変わらないことを意味する.
 * </p>
 *
 * <p>
 * リスト操作は次の三つのプリミティブによって記述することができる.
 * これらのプリミティブを提供するのは実装の責務である.
 * </p>
 * <dl>
 * <dt>{@link #head head}</dt>
 * <dd>リストの先頭要素を取得する.</dd>
 * <dt>{@link #tail tail}</dt>
 * <dd>{@code head} を除く全ての要素から成るリストを取得する.</dd>
 * <dt>{@link #isNil isNil}</dt>
 * <dd>このリストが {@code nil} (リストの終端を示す特別な空リスト) か否かをテストする.</dd>
 * </dl>
 *
 * <p>
 * このインターフェイスの static ファクトリメソッドはデフォルト実装のインスタンスを構築する.
 * </p>
 *
 * <p>
 * リストの要素として null 値を格納することはできない.
 * </p>
 *
 * @param <E> 要素の型
 * @see <a href="https://mitpress.mit.edu/sites/default/files/sicp/full-text/book/book-Z-H-15.html">
 *        <i>Structure and Interpretation of Computer Programs - 2.2 Hierarchical Data and the Closure Property</i>
 *      </a>
 * @see <a href="https://www.artima.com/pins1ed/working-with-lists.html">
 *        <i>Programming in Scala - Chapter 16 Working with Lists</i>
 *      </a>
 */
public interface List<E> extends Iterable<E> {

  //// static ファクトリメソッド ////

  /**
   * 与えられた先頭要素と後続リストからリストを構成する.
   *
   * @param  <T>  要素の型
   * @param  head 先頭要素
   * @param  tail 後続リスト
   * @return 先頭要素と後続リストから成るリスト
   * @throws NullPointerException 引数が {@code null} の場合
   */
  static <T> List<T> cons(T head, List<? extends T> tail) {
    return new ListImpl<>(head, tail);
  }

  /**
   * リスト終端マーカ ({@code nil}) を取得する.
   *
   * @param  <T> 要素の型
   * @return {@code nil}
   */
  @SuppressWarnings("unchecked") static <T> List<T> nil() {
    return (List<T>)ListImpl.NIL;
  }

  /**
   * 与えられた要素を含むリストを構成する.
   *
   * @param  <T> 要素の型
   * @param  xs  リストに含むべき要素の列
   * @return {@code xs} のリスト表現 ({@code xs} が空であれば {@code nil})
   */
  @SafeVarargs static <T> List<T> of(T... xs) {
    List<T> result = nil();

    for (var i = xs.length; 0<i;)
      result = cons(xs[--i], result);

    return result;
  }

  //// プリミティブ ////

  /**
   * 先頭要素を取得する.
   *
   * @return 先頭要素
   * @throws NoSuchElementException このリストが {@code nil} の場合
   */
  E head();

  /**
   * 先頭要素を除く全ての要素から成るサブリストを取得する.
   *
   * @return サブリスト
   * @throws UnsupportedOperationException このリストが {@code nil} の場合
   */
  List<E> tail();

  /**
   * リストが {@code nil} であるか否かをテストする.
   *
   * @return {@code nil} であれば真, そうでなければ偽
   */
  boolean isNil();

  //// 一階メソッド ////

  /**
   * イテレータを取得する.
   *
   * @return イテレータ
   */
  @Override default Iterator<E> iterator() {
    return new Iterator<>() {
      List<E> cur = List.this;

      @Override public boolean hasNext() {
        return !cur.isNil();
      }

      @Override public E next() {
        E e = cur.head();
        cur = cur.tail();
        return e;
      }
    };
  }

  /**
   * 要素数を取得する.
   *
   * @return 要素数
   */
  default int length() {
    return isNil() ? 0 : 1 + tail().length();
  }

  //// 高階メソッド ////

  /**
   * 述語関数を満たす要素を検索する.
   *
   * @param  predicate 述語関数
   * @return 述語関数を満たす要素
   * @throws NullPointerException 引数が {@code null} の場合
   */
  default Optional<E> find(Predicate<? super E> predicate) {
    return isNil() ?
             Optional.empty() :
             predicate.test(head()) ? Optional.of(head()) : tail().find(predicate);
  }

  /**
   * 各要素を述語関数でテストして選別する.
   *
   * @param  predicate 述語関数
   * @return 選別された要素から成るリスト
   * @throws NullPointerException 引数が {@code null} の場合
   */
  List<E> filter(Predicate<? super E> predicate);

  /**
   * 各要素に変換器を適用して写像する.
   *
   * @param  <R>        変換された要素の型
   * @param  transducer 変換器
   * @return 写像されたリスト
   * @throws NullPointerException 引数が {@code null} の場合
   */
  <R> List<R> map(Function<? super E, ? extends R> transducer);

  /**
   * 各要素にアクションを適用する.
   *
   * @param  action アクション
   * @throws NullPointerException 引数が {@code null} の場合
   */
  default void forEach(Consumer<? super E> action) {
    if (isNil())
      return;

    action.accept(head());
    tail().forEach(action);
  }

  /**
   * 各要素に積算器を適用する.
   * 初期値を開始時の積算結果としてリストの先頭から末尾へ向かって要素を積算する.
   * 即ち, 要素数を {@code N}, {@code accumulator} を左結合の二項中置演算子 {@code op} とすると
   * {@code initial op 要素1 op 要素2 op ... op 要素N} を計算する.
   *
   * @param  <R>         結果の型
   * @param  initial     初期値
   * @param  accumulator 第一引数に計算済みの積算結果を, 第二引数に要素を取る積算器
   * @return 積算結果
   * @throws NullPointerException {@code accumulator} が {@code null} の場合
   */
  default <R> R foldLeft(R initial, BiFunction<R, ? super E, R> accumulator) {
    return isNil() ? initial : tail().foldLeft(accumulator.apply(initial, head()), accumulator);
  }

  /**
   * 各要素に積算器を適用する.
   * 初期値を開始時の積算結果としてリストの末尾から先頭へ向かって要素を積算する.
   * 即ち, 要素数を {@code N}, {@code accumulator} を右結合の二項中置演算子 {@code op} とすると
   * {@code 要素1 op 要素2 op ... op 要素N op initial} を計算する.
   *
   * @param  <R>         結果の型
   * @param  initial     初期値
   * @param  accumulator 第一引数に要素を, 第二引数に計算済みの積算結果を取る積算器
   * @return 積算結果
   * @throws NullPointerException {@code accumulator} が {@code null} の場合
   */
  default <R> R foldRight(R initial, BiFunction<? super E, R, R> accumulator) {
    return isNil() ? initial : accumulator.apply(head(), tail().foldRight(initial, accumulator));
  }

}
