package ex9;
import java.util.ArrayList;
import java.util.stream.Stream;

public class ListEx4 {
	public static void main(String[] args) {
		show_stream();
	}

	public static void show_roop() {
		var li = new ArrayList<Staff>();
		li.add(new Staff("hitoshi", "kaihatu"));
		li.add(new Staff("nakamura", "kaihatu"));
		li.add(new Staff("nakajima", "kaihatu"));
		
		li.forEach(Staff::introduce);
	}
	
	public static void show_stream() {
		Stream.of(new Staff("hitoshi", "kaihatu"))
		.forEach(Staff::introduce);
	}
}
