package ex9;

import java.util.ArrayList;
import java.util.stream.Stream;
import java.util.stream.Collectors;

public class ListEx1 {
	public static void main(String[] args) {
		show_stream();
	}
	
	public static void show_roop() {
		var li = new ArrayList<String>();
		li.add("Java");
		li.add("C");
		li.add("C++");
		li.add("Ruby");
		li.add("PHP");
		li.add("Perl");
		
		li.forEach(System.out::println);
		System.out.println("length is " + li.size());		
	}
	
	public static void show_stream() {
		System.out.println("length is " +
				Stream.of("java","C","C++","Ruby","PHP","Perl")
				.peek(System.out::println)
				.collect(Collectors.toList())
				.size());
	}
}
