package ex9;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringEx1 {
	public static void main(String[] args) {
		show_regex();
	}
	
	public static void show_regex() {
			Matcher m = Pattern.compile("(\\d{2,4})-\\d{3,4}-\\d")
					.matcher(new Scanner(System.in).next());
			if(m.find()) {
				System.out.println("area is "+m.group("area"));				
			}
			
	}
	
	public static void show() {
		String phone = new Scanner(System.in).next();
		String shigai = phone.split("-")[0];
		System.out.printf("shigai kyokuban is %s\n", shigai);
	}
}
