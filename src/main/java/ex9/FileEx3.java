package ex9;

import java.io.File;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;
import java.util.stream.Stream;

public class FileEx3 {

	public static void main(String[] args) {
		func_new();
	}
	
	public static void func_new() {
		String root = "../../";
		System.out.print("root (from java_lib) :");
		var s = new Scanner(System.in);
		root += s.next();
		System.out.print("keyword :");
		String key = s.next();
		find_depth(root, key);
	}
	public static void func() {
		String root = "../../";
		System.out.print("root (from java_lib) :");
		var s = new Scanner(System.in);
		root += s.next();
		System.out.print("keyword :");
		String key = s.next();
		find_files(root, key).forEach(System.out::println);
	}
	
	public static Stream<String> find_files(String root, String key) {
		return Stream.of(new File(root).list())
				.filter(s -> s.contains(key));
	}
	
	public static void find_depth(String root, String key) {
		try(Stream<Path> entries = Files.walk(Paths.get(root))){
			entries.map(p -> p.getFileName().toString())
				.filter(s -> s.contains(key))
				.forEach(System.out::println);
		}catch(IOException ex) {
			throw new UncheckedIOException(ex);
		}
	}

}
