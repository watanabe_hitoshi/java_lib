package ex9;

public class Station {
	private int code;
	private String name;
	private double lat;
	private double lng;
	public Station(int code, String name, double lat, double lng) {
		code(code);
		name(name);
		lat(lat);
		lng(lng);
	}
	
	public void code(int c) {
		if(c<0) throw new IllegalArgumentException("invalid code");
		code = c;
	}
	
	public void name(String n) {
		name = n;
	}
	
	public void lat(double l) {
		if(l<0) throw new IllegalArgumentException("invalid lat");
		lat = l;
	}
	
	public void lng(double l) {
		if(l<0) throw new IllegalArgumentException("invalid lng");
		lng = l;
	}
	
	
	
	public int code() {
		return code;
	}
	
	public String name() {
		return name;
	}
	
	public double lat() {
		return lat;
	}
	
	public double lng() {
		return lng;
	}
	
	@Override
	public String toString() {
		return code + " : " + name;
	}
}
