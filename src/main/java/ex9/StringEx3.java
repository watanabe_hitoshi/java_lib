package ex9;

import java.util.Scanner;

public class StringEx3 {

	public static void main(String[] args) {
		while(true) {
			String s = new Scanner(System.in).next();
			if(s.equalsIgnoreCase("end")) {
				System.out.printf("inputed end sentence\n");
				break;
			}else {
				System.out.printf("your input is \"%s\". \n", s);
			}
		}
	}

}
