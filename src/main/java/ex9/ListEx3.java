package ex9;
import java.util.ArrayList;
import java.util.stream.Stream;

public class ListEx3 {
	public static void main(String[] args) {
		show_stream();
	}
	

	public static void show_roop() {
		var li = new ArrayList<Person>();
		li.add(new Person("htioshi"));
		li.add(new Person("nakamura"));
		li.add(new Person("nakajima"));
		
		li.forEach(Person::introduce);
	}
	
	public static void show_stream() {
			Stream.of(new Person("hitoshi"), new Person("nakajima"), new Person("kinoshita"))
			.forEach(Person::introduce);
	}
}
