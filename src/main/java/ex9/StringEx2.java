package ex9;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringEx2 {

	public static void main(String[] args) {
		func_regex();
	}
	
	public static void func() {
		String imp = new Scanner(System.in).next();
		String[] ps = imp.split(Pattern.quote("."));
		String cls = ps[ps.length-1];
		System.out.printf("class name is %s", cls);
	}
	
	public static void func_regex() {
			Pattern p = Pattern.compile("[^\\.]+$");
			Matcher m = p.matcher(new Scanner(System.in).next());
			m.find();
			m.group();
			
			
	}

}
