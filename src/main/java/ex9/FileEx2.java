package ex9;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

public class FileEx2 {

	public static void main(String[] args) throws IOException{
		func_new();
	}
	
	public static void func() {
		new File("../../writes/").mkdir();
		try(BufferedWriter bw = new BufferedWriter(
				new FileWriter(
				new File("../../writes/test.sql"), StandardCharsets.UTF_8))){
			for(int i=0;i<=100;i++)
				bw.write(String.format("SELECT * FROM MEMBER WHERE ID = %04d%n", i));
		}catch(FileNotFoundException ex) {
			ex.printStackTrace();
		}catch(IOException ex) {
			ex.printStackTrace();
		}
	}
	
	public static void func_new()throws IOException {
		var dir = Paths.get("..","..","writes");
		Files.createDirectories(dir);
		var path = dir.resolve(Paths.get("test.sql"));
		try(BufferedWriter br = Files.newBufferedWriter(path)){
			if(!Files.exists(path))
				Files.createFile(path);
			for(int i=1; i<=100; i++)
				br.write(String.format("SELECT * FROM MEMBER WHERE ID = %04d%n", i));
		}
	}
}