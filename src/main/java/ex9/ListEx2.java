package ex9;

import java.util.ArrayList;
import java.util.stream.DoubleStream;

public class ListEx2 {
	public static void main(String[] args) {
		show_stream();
	}
	
	public static void show_roop() {
		var li = new ArrayList<Double>();
		
		li.add(99.1);
		li.add(100.4);
		li.add(9937.34);
		li.add(1245.87);
		
		li.forEach(System.out::println);
		double res=0;
		for(double b : li) 
			res+=b;
		System.out.println("ave is " + res/li.size());
	}
	
	public static void show_stream() {
		System.out.println("ave is"+
			DoubleStream.of(99.9,10.3,34.5,87.5)
			.peek(System.out::println)
			.average()
			.orElse(0.0));
	}
	
	public static void show_regex() {
		
	}
}
