package ex9;
import java.util.HashMap;
import java.util.Scanner;

public class MapEx2 {
	public static void main(String[] args) {
		var map = new HashMap<Integer, Station>();
		map.put(1130224, new Station(1130224, "tokyo", 35.681391, 139.766103));
		
		while(true) {
			try {
				int code = new Scanner(System.in).nextInt();
				System.out.println(map.get(code));
				break;
			} catch (Exception e) {
				// TODO: handle exception
			}
		}
	}
}
