package ex9;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UncheckedIOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

public class FileEx1 {
	public static void main(String[] args) {
		func_new();
	}
	
	public static void func() {
		try(BufferedReader br = new BufferedReader(
				new InputStreamReader(
				new FileInputStream(
				new File("../../reads/test.csv")), StandardCharsets.UTF_8))){
			String line;
			while((line=br.readLine())!=null){
				Matcher m = Pattern.compile("(\\d+),([^,]+),([^,]+)").matcher(line);
				if(m.matches())
					System.out.printf("ID:%s name:%s from:%s\n", m.group(1), m.group(2), m.group(3));
			}		
		}catch(FileNotFoundException ex) {
			ex.printStackTrace();			
		}catch(IOException ex) {
			ex.printStackTrace();
		}
	}
	
	public static void func_new() {
		try(Stream<String> lines = Files.lines(Paths.get("..","..","reads","test.csv"))){
			var p = Pattern.compile("(\\d+),([^,]+),([^,]+)");
			lines.map(p::matcher)
				.filter(Matcher::matches)
				.forEach(m->System.out.printf(String.format("ID:%s name:%s from:%s\n", m.group(1), m.group(2), m.group(3))));
		}catch (IOException e) {
			throw new UncheckedIOException(e);
		}
	}
}
