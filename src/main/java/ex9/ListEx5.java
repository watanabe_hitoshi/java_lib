package ex9;

import java.util.List;
import java.util.stream.Collectors;


public class ListEx5 {

	public static void main(String[] args) {
		var ds = List.of(99.9, 100.4, 3000.5, 1400.2344444);
		ds.forEach(d -> System.out.printf("%,.2f\n", d));
		System.out.printf("average area is %,.2f\n", 
			ds.parallelStream()
				.collect(Collectors.summarizingDouble(Double::doubleValue))
				.getAverage());
	}

}
