package ex9;

public class Staff extends Person {
	private String role;
	public Staff(String name, String role) {
		super(name);
		this.role=role;
	}
	
	@Override
	public void introduce() {
		super.introduce();
		System.out.println("my role is " + role);
	}
}
