package ex9;
import java.util.HashMap;

import java.util.Scanner;

public class MapEx1 {
	public static void main(String[] args) {
		var map = new HashMap<String, String>();
		map.put("apple", "りんご");
		map.put("orange", "オレンジ");
		map.put("watermelon", "メロン");
		
		String s = null;
		try {
			s = new Scanner(System.in).next();
			System.out.println(map.get(s));		
		}catch(Exception e) {
			e.printStackTrace();
		}
	}	
}
